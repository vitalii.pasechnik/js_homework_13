"use strict";

/* Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.

    Функции setTimeout() и setInterval() принимают 2 параметра - это колбэк функция и задержка времени в миллисекундах.
    Разница между ними в том, что первая выполняет колбек функцию один раз после истечения указанной задержки времени, а 
    вторая - будет запускать колбэк функцию бесконечное количество раз, через указанный промежуток времени, 
    до тех пор пока этот цикл не остановят с помощью функции clearInterval().    
*/

/* Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

    Если передать нулевую задержку, то setTimeout() сработает сразу после выполнения всего остального кода на страцице.
    Т.е. таким образом мы можем создать некоторую асинхронность выполнения кода, заставив функцию "подождать" пока не выполнится остальной код на странице.
*/

/* 3. Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

    Функция setInterval() запускает бесконечный цикл который будет постоянно занимать память и нагружать процессор в фоновом режиме, 
    до тех пор пока мы не остановим этот цикл с помощью функции clearInterval();
    
*/

const imgWrapperNode = document.body.querySelector('.images-wrapper');
const nextBtn = document.body.querySelector('.btn-next');
const prevBtn = document.body.querySelector('.btn-prev');
const startBtn = document.body.querySelector('.btn-start');
const pauseBtn = document.body.querySelector('.btn-pause');
const timerLayout = document.body.querySelector('.layout-timer');

let imgNodes = [...imgWrapperNode.children];
imgWrapperNode.innerHTML = '';
imgNodes.forEach(img => imgWrapperNode.prepend(img));

nextBtn.addEventListener('click', () => showNext());
prevBtn.addEventListener('click', () => showPrev());
startBtn.addEventListener('click', () => autoPreview('start'));
pauseBtn.addEventListener('click', () => autoPreview('stop'));

let flag = true;

function showNext() {
    !flag && changeDirection();
    displayImg();
}

function showPrev() {
    flag && changeDirection();
    displayImg();
};

function changeDirection() {
    imgNodes = [imgNodes[0], ...imgNodes.slice(1).reverse()];
    imgNodes.forEach(img => imgWrapperNode.prepend(img));
    startBtn.classList.toggle('reverse');
    flag = !flag;
}

function disableEl(...nodes) {
    nodes.forEach(el => {
        el.classList.add('disabled');
        el.setAttribute('disabled', true);
    })
};

function setActiveEl(...nodes) {
    nodes.forEach(el => {
        el.classList.remove('disabled');
        el.removeAttribute('disabled');
    })
};

function displayImg(method) {
    if (!imgNodes[0].classList.contains('hide')) {
        disableEl(nextBtn, prevBtn, startBtn);
        imgNodes[0].classList.add('hide');
        setTimeout(() => {
            imgNodes.push(imgNodes.shift());
            imgNodes[imgNodes.length - 1].classList.remove('hide');
            imgNodes.forEach(img => imgWrapperNode.prepend(img));
            method !== "auto" && setActiveEl(nextBtn, prevBtn, startBtn);
        }, 1000);
    };
}

let preview;

function autoPreview(action) {
    if (action === 'start') {
        timer('start');
        preview = setInterval(() => displayImg("auto"), 3000);
        disableEl(nextBtn, prevBtn, startBtn);
        setActiveEl(pauseBtn);
    } else if (action === 'stop') {
        timer('stop');
        clearInterval(preview);
        setActiveEl(nextBtn, prevBtn, startBtn);
        disableEl(pauseBtn);
    };
}

let playback;

function timer(action) {
    if (action === 'start') {
        let delaySec = 3;
        timerLayout.textContent = `${delaySec--}`;
        timerLayout.classList.add('active');
        playback = setInterval(() => {
            timerLayout.textContent = `${delaySec--}`;
            if (delaySec < 0) {
                timerLayout.textContent = `3`;
                delaySec = 2;
            }
        }, 1000);

    } else if (action === 'stop') {
        timerLayout.textContent = '';
        timerLayout.classList.remove('active');
        clearInterval(playback);
    }
}